package com.example.lab2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private ListView listView;
    private EditText editText;
    private Button addButton;
    private Button deleteButton;
    private Button saveButton;
    private Button loadButton;
    private ArrayList<String> list= new ArrayList();;
    private ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        list.add("Reading");
        list.add("Training");
        list.add("Cooking");

        adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_multiple_choice, list);
        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.remove(adapter.getItem(position));
                return false;
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(editText.getText().toString());
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save("done.txt");
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load("done.txt");
            }
        });
    }

    private void load(String s) {
        try {
            InputStreamReader sR = new InputStreamReader(openFileInput("done.txt"));
            BufferedReader bR = new BufferedReader(sR);
            String line;
            while((line = bR.readLine()) != null) {
                list.add(line);
            }
            sR.close();

        }
        catch (IOException e) {
            Toast.makeText(getApplicationContext(),
                    e.getMessage(),
                    Toast.LENGTH_LONG).show();

        }
        adapter.clear();
        adapter.addAll(list);
    }

    private void save(String s) {
        try {
            OutputStreamWriter oS = new OutputStreamWriter(openFileOutput("done.txt",
                    Context.MODE_PRIVATE));
            for (int i = 0; i < adapter.getCount(); i++) {
                oS.write(adapter.getItem(i) + '\n');
            }
            oS.close();

        }
        catch (IOException e) {
            Toast.makeText(getApplicationContext(),
                    e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void delete() {
        if (listView.getCheckedItemCount() > 0)
        {
            SparseBooleanArray checked = listView.getCheckedItemPositions();
            for (int i = checked.size() -1; i >= 0; i--) {
                adapter.remove(adapter.getItem(checked.keyAt(i)));
            }
            listView.clearChoices();
        }
    }


    private void add(String element) {
        adapter.add(element);
            }

    private void init() {
        listView = findViewById(R.id.listView);
        editText = findViewById(R.id.editText);
        addButton = findViewById(R.id.addButton);
        deleteButton = findViewById(R.id.deleteButton);
        saveButton = findViewById(R.id.saveButton);
        loadButton = findViewById(R.id.loadButton);
    }
}
